import { createStore } from 'vuex'
import { colorsStore } from './colors-store'
import { contextMenuStore } from './context-menu-store'
import { boardsStore } from './boards-store'
import { leftPanelStore } from './left-panel-store'
import { modalStore } from './modal-store'
import { mouseTrackingStore } from './mouse-tracking-store'

export const store = createStore({
	modules: {
		colorsStore,
		contextMenuStore,
		boardsStore,
		leftPanelStore,
		modalStore,
		mouseTrackingStore
	}
})
