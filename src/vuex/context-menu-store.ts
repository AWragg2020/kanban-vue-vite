export const contextMenuStore = {
	state() {
		return {
			coordinates: { x: 0, y: 0 } as Record<string, number>,
			isOpen: false,
			options: [] as Array<string>
		}
	},
	mutations: {
		setCoordinates(state: any, coordinates: Record<string, number>) {
			state.coordinates = coordinates
		},
		setIsOpen(state: any, isOpen: boolean) {
			state.isOpen = isOpen
		},
		setOptions(state: any, options: Array<string>) {
			state.options = options
		}
	},
	actions: {
		showContextMenu({ commit }: any, { coordinates, options }: Record<string, any>) {
			commit('setCoordinates', coordinates)
			commit('setOptions', options)
			commit('setIsOpen', true)
		},
		hideContextMenu({ commit }: any) {
			commit('setCoordinates', { x: 0, y: 0 })
			commit('setOptions', [])
			commit('setIsOpen', false)
		}
	}
}
