import { Board, Column, ElementTypes, ModalModes, Task } from '../types'

const generateRandomID = () => {
	/*
		For a collision in a category, there would have to be about 9.2*10^18 
		elements in that category... Safe to assume there won't be any collisions 
		for presentational purposes :D
	*/
	return Math.floor(Math.random() * Math.pow(10, 16)).toString(16)
}

export const boardsStore = {
	state() {
		return {
			selectedBoard: {} as Board,
			elementToModify: { id: '', type: '' } as Record<string, string>,
			boards: (JSON.parse(localStorage.getItem('boards') as string) as Array<Board>) || [],
			columns: (JSON.parse(localStorage.getItem('columns') as string) as Array<Column>) || [],
			tasks: (JSON.parse(localStorage.getItem('tasks') as string) as Array<Task>) || []
		}
	},
	mutations: {
		setElementToModify(state: any, { id, type }: Record<string, string>) {
			state.elementToModify = { id, type }
		},
		setSelectedBoard(state: any, boardID: string) {
			state.selectedBoard = boardID
		},
		addNewBoard(state: any, title: string) {
			const newBoard = {
				title,
				id: generateRandomID()
			} as Board

			const newBoards = [...state.boards, newBoard]
			localStorage.setItem('boards', JSON.stringify(newBoards))
			state.boards = newBoards
		},
		editBoard(state: any, newTitle: string) {
			state.boards.find((board: Board) => board.id === state.elementToModify.id).title = newTitle
			localStorage.setItem('boards', JSON.stringify(state.boards))
		},
		deleteBoard(state: any) {
			const boardID = state.elementToModify.id
			const updatedColumns = state.columns.filter((column: Column) => column.boardID !== boardID)
			const updatedTasks = state.tasks.filter((task: Task) =>
				updatedColumns.map((column: Column) => column.id).includes(task.columnID)
			)

			state.tasks = updatedTasks
			state.columns = updatedColumns
			state.boards = state.boards.filter((board: Board) => board.id !== boardID)

			state.elementToModify = { id: '', type: '' }

			localStorage.setItem('tasks', JSON.stringify(updatedTasks))
			localStorage.setItem('columns', JSON.stringify(updatedColumns))
			localStorage.setItem('boards', JSON.stringify(state.boards))
		},
		addNewColumn(state: any, { title, decoratorColor, boardID }: Record<string, any>) {
			const newColumn = {
				title,
				id: generateRandomID(),
				decoratorColor,
				boardID
			} as Column

			const newColumns = [...state.columns, newColumn]
			localStorage.setItem('columns', JSON.stringify(newColumns))
			state.columns = newColumns
		},
		editColumn(state: any, { title, decoratorColor }: Record<string, any>) {
			const columnToModify = state.columns.find(
				(column: Column) => column.id === state.elementToModify.id
			) as Column
			columnToModify.title = title
			columnToModify.decoratorColor = decoratorColor

			localStorage.setItem('columns', JSON.stringify(state.columns))
		},
		deleteColumn(state: any) {
			const columnID = state.elementToModify.id
			const updatedTasks = state.tasks.filter((task: Task) => task.columnID !== columnID)

			state.tasks = updatedTasks
			state.columns = state.columns.filter((column: Column) => column.id !== columnID)

			state.elementToModify = { id: '', type: '' }

			localStorage.setItem('tasks', JSON.stringify(updatedTasks))
			localStorage.setItem('columns', JSON.stringify(state.columns))
		},
		addNewTask(state: any, { title, description, columnID, parentTaskID }: Record<string, any>) {
			const newTask = {
				title,
				description,
				id: generateRandomID(),
				columnID: columnID ?? '',
				parentTaskID: parentTaskID ?? ''
			}
			const newTasks = [...state.tasks, newTask]
			state.tasks = newTasks
			localStorage.setItem('tasks', JSON.stringify(newTasks))
		},
		editTask(state: any, { title, description }: Record<string, any>) {
			const taskToModify = state.tasks.find((task: Task) => task.id === state.elementToModify.id) as Task
			taskToModify.title = title
			taskToModify.description = description

			localStorage.setItem('tasks', JSON.stringify(state.tasks))
		},
		deleteTask(state: any) {
			const taskID = state.elementToModify.id
			state.tasks = state.tasks.filter((task: Task) => task.id !== taskID && task.parentTaskID !== taskID)

			state.elementToModify = { id: '', type: '' }
			localStorage.setItem('tasks', JSON.stringify(state.tasks))
		},
		moveTask(state: any, columnID: string) {
			state.tasks.find((task: Task) => task.id === state.elementToModify.id).columnID = columnID
			localStorage.setItem('tasks', JSON.stringify(state.tasks))
		},
		dragTask(state: any, { taskID, columnID }: Record<string, string>) {
			state.tasks.find((task: Task) => task.id === taskID).columnID = columnID
			localStorage.setItem('tasks', JSON.stringify(state.tasks))
		}
	},
	actions: {
		setElementToModify({ commit }: any, { id, type }: Record<string, any>) {
			commit('setElementToModify', { id, type })
		},
		selectBoard({ commit }: any, boardID: string) {
			commit('setSelectedBoard', boardID)
		},
		createNewBoard({ commit }: any, title: string) {
			commit('addNewBoard', title)
		},
		editBoard({ commit }: any, newTitle: string) {
			commit('editBoard', newTitle)
		},
		deleteBoard({ commit }: any) {
			commit('deleteBoard')
		},
		createNewColumn({ commit }: any, { title, decoratorColor, boardID }: Record<string, any>) {
			commit('addNewColumn', { title, decoratorColor, boardID })
		},
		editColumn({ commit }: any, { title, decoratorColor }: Record<string, any>) {
			commit('editColumn', { title, decoratorColor })
		},
		deleteColumn({ commit }: any) {
			commit('deleteColumn')
		},
		createNewTask({ commit }: any, { title, description, columnID }: Record<string, any>) {
			commit('addNewTask', { title, description, columnID })
		},
		createSubtask({ commit }: any, { title, description, parentTaskID }: Record<string, any>) {
			commit('addNewTask', { title, description, parentTaskID })
		},
		editTask({ commit }: any, { title, description }: Record<string, any>) {
			commit('editTask', { title, description })
		},
		deleteTask({ commit }: any) {
			commit('deleteTask')
		},
		moveTask({ commit }: any, columnID: string) {
			commit('moveTask', columnID)
		},
		dragTask({ commit }: any, { taskID, columnID }: Record<string, string>) {
			commit('dragTask', { taskID, columnID })
		}
	}
}
