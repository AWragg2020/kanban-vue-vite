export const colorsStore = {
	state() {
		return {
			colors: {
				green: '#BFD7B5',
				greenishBlue: '#41A6B5',
				purple: '#9E4770',
				darkPurple: '#5C3E7A',
				blue: '#7DCFFF',
				ochre: '#EACDC2'
			}
		}
	}
}
