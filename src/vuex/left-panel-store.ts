export const leftPanelStore = {
	state() {
		return {
			isHidden: false
		}
	},
	mutations: {
		setIsHidden(state: any, isHidden: boolean) {
			state.isHidden = isHidden
		}
	},
	actions: {
		hideLeftPanel({ commit }: any) {
			commit('setIsHidden', true)
		},
		showLeftPanel({ commit }: any) {
			commit('setIsHidden', false)
		}
	}
}
