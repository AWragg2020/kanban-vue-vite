const initialState = {
	dragOffset: { x: 200, y: 28 },
	mousePosition: { x: 0, y: 0 },
	overColumnId: '',
	isDragging: false
}

export const mouseTrackingStore = {
	state() {
		return {
			...initialState
		}
	},
	mutations: {
		setMousePosition(state: typeof initialState, { x, y }: Record<string, number>) {
			state.mousePosition = { x, y }
		},
		setOverColumnId(state: typeof initialState, columnId: string) {
			state.overColumnId = columnId
		},
		setIsDragging(state: typeof initialState, isDragging: boolean) {
			state.isDragging = isDragging
		}
	},
	actions: {
		updateMousePosition({ commit }: any, { x, y }: Record<string, number>) {
			commit('setMousePosition', { x, y })
		},
		setOverColumnId({ commit }: any, columnId: string) {
			commit('setOverColumnId', columnId)
		},
		setIsDraggingTask({ commit }: any, isDragging: boolean) {
			commit('setIsDragging', isDragging)
		}
	}
}
