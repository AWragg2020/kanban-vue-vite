import { ModalModes } from '../types'

export const modalStore = {
	state() {
		return {
			showBoardModal: false,
			showColumnModal: false,
			showTaskModal: false,
			showDeleteModal: false,
			showMoveModal: false,
			boardModalMode: 'create' as ModalModes,
			columnModalMode: 'create' as ModalModes,
			taskModalMode: 'create' as ModalModes
		}
	},
	mutations: {
		setShowBoardModal(state: any, { showModal, mode }: Record<string, any>) {
			state.boardModalMode = mode ?? state.boardModalMode
			state.showBoardModal = showModal
		},
		setShowColumnModal(state: any, { showModal, mode }: Record<string, any>) {
			state.columnModalMode = mode ?? state.columnModalMode
			state.showColumnModal = showModal
		},
		setShowTaskModal(state: any, { showModal, mode }: Record<string, any>) {
			state.taskModalMode = mode ?? state.taskModalMode
			state.showTaskModal = showModal
		},
		setShowDeleteModal(state: any, showModal: boolean) {
			state.showDeleteModal = showModal
		},
		setShowMoveModal(state: any, showModal: boolean) {
			state.showMoveModal = showModal
		}
	},
	actions: {
		showBoardModal({ commit }: any, mode: ModalModes) {
			commit('setShowBoardModal', { showModal: true, mode: mode } as Record<string, boolean | string>)
		},
		hideBoardModal({ commit }: any) {
			commit('setShowBoardModal', { showModal: false } as Record<string, boolean>)
		},
		showColumnModal({ commit }: any, mode: ModalModes) {
			commit('setShowColumnModal', { showModal: true, mode: mode } as Record<string, boolean | string>)
		},
		hideColumnModal({ commit }: any) {
			commit('setShowColumnModal', { showModal: false } as Record<string, boolean>)
		},
		showTaskModal({ commit }: any, mode: ModalModes) {
			commit('setShowTaskModal', { showModal: true, mode: mode } as Record<string, boolean | string>)
		},
		hideTaskModal({ commit }: any) {
			commit('setShowTaskModal', { showModal: false } as Record<string, boolean>)
		},
		showDeleteModal({ commit }: any) {
			commit('setShowDeleteModal', true)
		},
		hideDeleteModal({ commit }: any) {
			commit('setShowDeleteModal', false)
		},
		showMoveModal({ commit }: any) {
			commit('setShowMoveModal', true)
		},
		hideMoveModal({ commit }: any) {
			commit('setShowMoveModal', false)
		}
	}
}
