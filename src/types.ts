export interface Board {
	title: string
	id: string // 32 chars, HEX
}

export interface Column {
	title: string
	decoratorColor: string
	id: string
	boardID: string
}

export interface Task {
	title: string
	description: string
	id: string
	columnID: string
	parentTaskID: string
}

export type ElementTypes = 'board' | 'column' | 'task' | ''
export type ModalModes = 'create' | 'edit' | 'delete' | 'move' | 'addSubtask'
